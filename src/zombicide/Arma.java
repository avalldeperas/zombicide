
package zombicide;


public class Arma {
    // by default --> SARTEN
    private int danyo = 1;
    private int distancia = 1;
    private TipoArma tipoArma = TipoArma.SARTEN;

    public Arma() {
    }
    
    public Arma(TipoArma tipoArma) {
        setTipoArma(tipoArma);
    }
    
    public int getDanyo() {
        return danyo;
    }

    public void setDanyo(int danyo) {
        this.danyo = danyo;
    }

    public int getDistancia() {
        return distancia;
    }

    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }

    public TipoArma getTipoArma() {
        return tipoArma;
    }

    public void setTipoArma(TipoArma tipoArma) {
        this.tipoArma = tipoArma;
        switch(tipoArma){
            case SARTEN:
                this.danyo = 1;
                this.distancia = 1;
                break;
            case PISTOLA:
                this.danyo = 1;
                this.distancia = 2;
                break;
            case ESCOPETA:
                this.danyo = 1;
                this.distancia = 2;
                break;
            case KATANA:
                this.danyo = 2;
                this.distancia = 1;
                break;
            case HACHA:
                this.danyo = 2;
                this.distancia = 1;
        }
    }
    
    public void armarse(){
        System.out.println("COWABUNGA!");
    }

    @Override
    public String toString() {
        return "Arma: " + tipoArma + "\n\t\tDaño: " + danyo + "\n\t\tDistancia: " + distancia;
    }
    
    
}
