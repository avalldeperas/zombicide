package zombicide;

public class Witch extends Zombie{
    private static int GOLPEOMAXIMOWITCH;
    
    public Witch(){
        super(3, 2, TipoZombie.WITCH, 2);
    }

    public Witch(int danyo, int movimiento, TipoZombie tipo) {
        super(danyo, movimiento, tipo.WITCH, 2);
    }

    @Override
    public int getVidas() {
        return super.getVidas(); //To change body of generated methods, choose Tools | Templates.
    }

    public static int getGolpeMaximoWitch() {
        return GOLPEOMAXIMOWITCH;
    }

    public static void setGolpeMaximoWitch(int golpeMaximoWitch) {
        Witch.GOLPEOMAXIMOWITCH = golpeMaximoWitch;
    }
    
    @Override
    public int calcularGolpeo(int numAleatorio, Superviviente superviv) {
         boolean maxZombies = false, maxWitch = false;
        int golpeo = super.calcularGolpeo(numAleatorio, superviv) * getDanyo(); //To change body of generated methods, choose Tools | Templates.
        
    // GOLPEO MAXIMO DE ZOMBIES
        if(golpeo > getMaxGolpeo()){
            setMaxGolpeo(golpeo);
            maxZombies = true;
        }
        
        // GOLPEO MÁXIMO DE TODOS LOS ZOMBIES
        if(golpeo > getGolpeMaximoWitch()){
            setGolpeMaximoWitch(golpeo);
            maxWitch = true;
        }
        // GOLPEO MÁXIMO DE WITCHS
        if(maxWitch && maxZombies){
            System.out.println("\tLa " + getClass().getSimpleName() + " ha establecido un nuevo récord de golpeo de TODOS los ZOMBIES!");
        } else if (maxWitch && !maxZombies){
            System.out.println("\tLa " + getClass().getSimpleName() + " ha establecido un nuevo récord de todos las " + getClass().getSimpleName());
        }
        return golpeo;
    }
    
    public boolean zarpazo(){
        if(MetodosSueltos.generaNumAleatorio(1, 10) > 7){
            System.out.println("\tOH NO! The " + getClass().getSimpleName() + " ataca DOS veces...");
            return true;
        } else {
            System.out.println("\tHabéis tenido suerte y The " +getClass().getSimpleName() + " ataca una vez.");
            return false;
        }
    }
}
