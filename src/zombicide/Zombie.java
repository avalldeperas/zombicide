
package zombicide;

public class Zombie {
    
    private int danyo = 0;
    private int movimiento = 0;
    private TipoZombie tipoZombie;
    private int nivel = 0;
    private int vidas = 1;
    private static int MAXGOLPEO;

    public Zombie() {
        new Horda();
    }
    
    public Zombie(int danyo, int movimiento, TipoZombie tipo, int vidas){
        this.danyo = danyo;
        this.movimiento = movimiento;
        this.tipoZombie = tipo;
        this.vidas = vidas;
    }
    
    public int getVidas() {
        return vidas;
    }

    public void setVidas(int vidas) {
        this.vidas = vidas;
    }
    
    public int getDanyo() {
        return danyo;
    }

    public void setDanyo(int danyo) {
        this.danyo = danyo;
    }

    public int getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(int movimiento) {
        this.movimiento = movimiento;
    }

    public TipoZombie getTipoZombie() {
        return tipoZombie;
    }

    public void setTipoZombie(TipoZombie tipoZombie) {
        this.tipoZombie = tipoZombie;
    }

    @Override
    public String toString() {
        return "\tDaño: " + danyo + "\n\tMovimiento:" + movimiento + "\n\tTipoZombie:" + tipoZombie;
    }
    
    public int calcularGolpeo(int numAleatorio, Superviviente superviv){
        int golpeo = 10;
            switch(superviv.getSkill()){
                case RAPIDO:
                    golpeo *= 5;
                    break;
                case BUSCADOR:
                    golpeo *= 3;
                    break;
                case FORTACHON:
                    golpeo *= 7;
                    break;
                case AMBIDIESTRO:
                    golpeo *= 8;
                    break;
                case ESCURRIDIZO:
                    golpeo *= 9;
                    break;
            }
        return golpeo;
    }

    public void setMaxGolpeo(int maxGolpeo) {
        this.MAXGOLPEO = maxGolpeo;
    }

    public int getMaxGolpeo() {
        return MAXGOLPEO;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
    
    
}
