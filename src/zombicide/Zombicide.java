
package zombicide;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.util.ArrayList;

public class Zombicide {
    // ARRAY LISTS
    static ArrayList <Zombie> listaZombies = new ArrayList<>();
    static ArrayList <Superviviente> listaSupervivientes = new ArrayList<>(5);
    static ArrayList <Arma> listaArmas = new ArrayList<>(5);
    static char[] arrayZombies;
    static char[] arraySuperviv;
    private static String namePlayer;
            
    // BUFFER & SCANNER
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static Scanner sc = new Scanner(System.in);
    
    public static void main(String[] args) {
        int decision = -1, nivelDificultad;
        boolean checker = false;
        // SETEAMOS EL JUEGO
        nivelDificultad = settingAllGame();
        
        // EMPIEZA EL JUEGO
        do{
            if(!checker){
                System.out.print(namePlayer+ ", empezamos el juego? [0 - No | 1 - Si ]: ");
                checker = true;
            } else {
                System.out.println("No puedes escapar...");
                System.out.print("Empezamos el juego? [1 - Si ]: ");
            }
            decision = sc.nextInt();
        }while(decision != 1);
        
        empiezaJuego(nivelDificultad);
       
    }
    
    public static void empiezaJuego(int nivelDificultad){
         int zombiesMatados = 0, humanosMatados = 0, plantas = 3, opcion = -1;
         boolean supervivMuertos = false, checker = false;
         
         // hay que hacerla fuera porque será la misma para todas las plantas!
         llenarArrayCharSupervivientes();
         
         for (int i = 0; i < plantas; i++){
            System.out.println("\n\n/*-*-*-*-*-*      PLANTA " + (i + 1) + "     *-*-*-*-*-*/");
            
            creatingZombieListByLevel(i, nivelDificultad);
            // llenamos el array de chars con la primera letra de la clase de zombie.
            llenarArrayCharZombies();
            do {
                zombiesMatados = turnoSupervivientes(zombiesMatados, listaZombies);
                if(zombiesMatados < listaZombies.size() && humanosMatados < listaSupervivientes.size()){
                    humanosMatados = turnoZombies(humanosMatados, listaZombies);
                } 
                // en caso que todos los supervivientes hayan muerto       
                if(humanosMatados == listaSupervivientes.size()){
                    i = plantas;
                    supervivMuertos = true;
                }
            } while(zombiesMatados < listaZombies.size() && humanosMatados < listaSupervivientes.size());
            
            // MENU PARA SIGUIENTE PLANTA
            if(i < 2){
                do{
                    if(checker){
                        System.out.println("No te puedes escapar...");
                    }
                    System.out.print("Pulsa 1 para subir a la  planta " + (i + 2)+": ");
                    opcion = sc.nextInt();
                    checker = true;
                }while(opcion != 1);
                checker = false;
            }
            // reset de los zombies para la siguiente planta
            zombiesMatados = 0;
        }
         if(!supervivMuertos){
             System.out.println("\nFELICIDADES!!! Habéis ganado la partida!!!!");
         } else {
             System.out.println("\nGAME OVER. Los Zombies os han masacrado...");
         }
         
         // RESUMEN ESTADÍSTICAS
         printEstadisticasSupervivientes(humanosMatados);
    }
    
    public static int turnoSupervivientes(int zombiesMuertos, ArrayList <Zombie> listaDeZombies ){
        int tiradas, counter = zombiesMuertos, eleccionJugador = 0;
        Superviviente player; 
        boolean checker = false, mensajeYaMostrado = false;
        
        System.out.println("\n|<--|<-- A LAS ARMAS supervivientes! Es vuestro turno! -->|-->|");
        
        // printamos el array de chars de zombies para el jugador
        System.out.println("\nAhora mismo esta es la distribución para esta planta: ");
        printArrayCharZombies();
        printArrayCharSupervivientes();
        
        preguntarAPlayerSiContinuar(", tu y tu equipo estais preparados para atacar?");
        
        for (int i = 0; i < listaSupervivientes.size(); i++){
            mensajeYaMostrado = false;
            player = listaSupervivientes.get(i);
            if(player.getVidas() > 0){
                System.out.println("\n->Turno de Superviviente nº" + (i + 1) +": " + player.getNombre());
                if(player.getSkill() == SkillsPersonaje.AMBIDIESTRO && player.getArma().getTipoArma() == TipoArma.PISTOLA){
                    tiradas = 6;
                } else {
                    tiradas = 3;
                }
                // ATAQUE DEL SUPERVIVIENTE numero i
                for(int j = 0; j < tiradas; j++){
                    if(player.getNombre().equalsIgnoreCase(namePlayer)){
                        if(!checker){
                            if(player.getArma().getDanyo() < 2){
                                System.out.println("\tRecuerda que tu arma tiene daño " + player.getArma().getDanyo() + " y solo podrás matar a los zombies tipo " + TipoZombie.CAMINANTE);
                            } else {
                                System.out.println("\tTu arma tiene daño " + player.getArma().getDanyo() + " así que podrás matar cualquier tipo de ZOMBIE... a no ser que falles...");
                            }
                            checker = true;
                        }
                        eleccionJugador = ataqueInputPlayer();
                    }
                    System.out.println("Ataque " + j);
                    switch(MetodosSueltos.generaNumAleatorio(0, 1)){
                        // FALLO
                        case 0:
                            if(!mensajeYaMostrado){
                                System.out.println(mensajeFallo(player));
                                mensajeYaMostrado = true;
                            } else {
                                System.out.println("\tAtaque FALLIDO otra vez...");
                            }
                            player.setNumFallos(player.getNumFallos()+1);
                            if(counter == listaDeZombies.size()){
                               j = tiradas;
                               i = listaSupervivientes.size();
                               System.out.println("\nBUENA! Los Supervivientes han matado a TODOS los Zombies de esta planta!!!\n");
                            }
                            break;
                        // ENTRA HIT
                        case 1:
                            // buscamos si hay un zombie vivo
                            if(!player.getNombre().equalsIgnoreCase(namePlayer)){
                                for(int z = 0; z < listaDeZombies.size(); z++){
                                    if(listaDeZombies.get(z).getVidas() > 0){
                                        // en caso que DAÑO supervivientes sea mayor o igual a VIDAS de zombies
                                        if(player.getArma().getDanyo() >= listaDeZombies.get(z).getVidas()){
                                            listaDeZombies.get(z).setVidas(0);
                                            player.setZombiesMatados(player.getZombiesMatados()+1);
                                            arrayZombies[z] = 'M';
                                            counter++;
                                            System.out.println("\tBIEN! " + listaDeZombies.get(z).getTipoZombie() + " MUERTO. Quedan " + (listaDeZombies.size() - counter) + " zombies.");
                                            z = listaDeZombies.size();
                                        } else {
                                            System.out.println("\tVAYA! La " + player.getArma().getTipoArma() + " no tiene suficiente ataque para matar un " + listaDeZombies.get(z).getTipoZombie());
                                            z = listaDeZombies.size();
                                            player.setNumFallos(player.getNumFallos()+1);
                                        }
                                    } 
                                }
                            } else {
                                if(listaDeZombies.get(eleccionJugador).getVidas() > 0){
                                        // en caso que DAÑO supervivientes sea mayor o igual a VIDAS de zombies
                                        if(player.getArma().getDanyo() >= listaDeZombies.get(eleccionJugador).getVidas()){
                                             player.setZombiesMatados(player.getZombiesMatados()+1);
                                            listaDeZombies.get(eleccionJugador).setVidas(0);
                                            arrayZombies[eleccionJugador] = 'M';
                                            counter++;
                                            System.out.println("\tBIEN! " + listaDeZombies.get(eleccionJugador).getTipoZombie() + " MUERTO. Quedan " + (listaDeZombies.size() - counter) + " zombies.");
                                        } else {
                                            System.out.println("\tVAYA! La " + player.getArma().getTipoArma() + " no tiene suficiente ataque para matar un " + listaDeZombies.get(eleccionJugador).getTipoZombie());
                                        }
                                    } 
                            }
                            // si todos los zombies estan muertos
                            if(counter == listaDeZombies.size()){
                               j = tiradas;
                               i = listaSupervivientes.size();
                               System.out.println("\nBUENA! Los Supervivientes han matado a TODOS los Zombies de esta planta!!!\n");
                            }
                    }
                }
                // si es el usuario al que le toca y no estan todos los zombies muertos aún, pregunta cambio de turno
                if(player.getNombre().equalsIgnoreCase(namePlayer) && counter < listaDeZombies.size()){
                    preguntarAPlayerSiContinuar(", quieres pasar turno?");
                }
            }
        }
        return counter;
    }
    
    // metódo que pregunta al jugador si quiere continuar o no, modificando el mensaje que recibe como parámetro.
    public static void preguntarAPlayerSiContinuar(String mensaje){
        int finalizarTurno = 0;
        boolean checker = false;
        
        System.out.print("\n" + namePlayer + mensaje + "[0 - No][1 - Sí]: ");
        do{
            if(checker){
                System.out.print("Pulsa 1 cuando estés listo... ");
            }
            finalizarTurno = sc.nextInt();
            checker = true;
        }while(finalizarTurno != 1);
    }
    
    public static int ataqueInputPlayer(){
        int eleccionPlayer;
        boolean checker = false;
        do{
            printArrayCharZombies();
            do{
                System.out.print("\t" + namePlayer + ", escribe el numero del zombie al que quieras atacar: ");
                eleccionPlayer = sc.nextInt();
            }while(eleccionPlayer < 0 || eleccionPlayer > listaZombies.size()-1);
            if(eleccionPlayer > 0 || eleccionPlayer < listaZombies.size() - 1){
                if(arrayZombies[eleccionPlayer] != 'M'){
                    checker = true;
                }  else {
                    System.out.println("Este Zombie ya está muerto!");
                }
            } else {
                System.out.println("Numero incorrecto...");
            }
        }while(!checker);
        
        return eleccionPlayer;
    }
    
    public static int turnoZombies(int humanosMatados, ArrayList <Zombie> listaDeZombies){
        int random, counter = humanosMatados, vecesAtaca = 1;
        Superviviente player;
        boolean checker = false;
        
        System.out.println("\n\n|-->|-->|A DEFENDER! Ahora atacan los zombies!!<--|<--|");
        preguntarAPlayerSiContinuar(", estáis preparados para defender?");
        
        for (int i = 0; i < listaDeZombies.size(); i++){
            if(listaDeZombies.get(i).getVidas() > 0){
                System.out.println("\nEs turno del " + listaDeZombies.get(i).getTipoZombie() + " en la posicion " + i);
                
                do{
                    for (int j = 0; j < listaSupervivientes.size(); j++){
                        random = MetodosSueltos.generaNumAleatorio(0, listaSupervivientes.size()-1);
                        player = listaSupervivientes.get(random);
                        if(player.getVidas() > 0){
                            // no puede escaparse
                            if(!player.escaparse()){
                                // witch  mata de un golpe
                                if(listaDeZombies.get(i) instanceof Witch){
                                    if(!checker){
                                        Witch aux = (Witch) listaDeZombies.get(i);
                                        if(aux.zarpazo()){
                                            vecesAtaca = 2;
                                        }
                                        checker = true;
                                    }
                                   player.setVidas(0);     
                                } else {
                                    player.setVidas(player.getVidas() - listaDeZombies.get(i).getDanyo());
                                }
                                // en caso que lo maten
                                if(player.getVidas() < 1){
                                    player.setVidas(0);
                                    arraySuperviv[random] = 'M';
                                    System.out.println("\tOHHH! Han matado a " + player.getNombre() + "!");
                                    counter++;
                                } else {
                                    System.out.println("\tOJO! Han mordido a " + player.getNombre() + "! Le quedan " + player.getVidas() + " vidas.");
                                }
                                // calcular golpeo, no hace falta hace instanceOf ya que llama primero a la subclasse!
                                System.out.println("\tEl golpeo ha sido de " + listaDeZombies.get(i).calcularGolpeo(MetodosSueltos.generaNumAleatorio(0, 10), player) + " puntos.");
                                
                                // zombie ya ha mordido a alguien
                                j = listaSupervivientes.size();
                                
                                // todos los supervivientes muertos
                                if(counter == listaSupervivientes.size()){
                                    i = listaDeZombies.size();
                                }
                            } else {
                                System.out.println("\t"+player.getNombre() + " se puede escapar por tener skill " + player.getSkill() + "!");
                                j = listaSupervivientes.size();
                            }
                        // caso que superviviente esté muerto
                        } else {
                            // todos los supervivientes muertos
                            if(counter == listaSupervivientes.size()){
                                j = listaSupervivientes.size();
                                i = listaDeZombies.size();
                            // si aún hay algún superviviente vivo, zombie vuelve a buscar superviviente que no esté muerto
                            } else {
                                j -= 1;
                            }
                        }
                    }
                    vecesAtaca--;
                }while(vecesAtaca >= 1);
            }
        }
        return counter;
    }
    
    public static int settingAllGame(){
        int posDobleTurno = 0, posAmbidiestro = 0, posNoAmbidiestro = 0, nivelDificultad = 0, posBuscador = 0, abrirBaul = 0;
        boolean cambioDeArmas = false;
        TipoArma armaAnterior = null;
        
        System.out.println("                           /-*-/");
        System.out.println("                      /-*-/-*-/-*-/");
        System.out.println("                 /-*-/-*-/-*-/-*-/-*-/");
        System.out.println("            /-*-/-*-/-*-/-*-/-*-/-*-/-*-/");
        System.out.println("       /-*-/-*-/ BIENVENIDO A ZOMBICIDE/-*-/-*-/");
        System.out.println("          /-*-/-*-/-*-/-*-/-*-/-*-/-*-/-*-/");
        System.out.println("             /-*-/-*-/-*-/-*-/-*-/");
        System.out.println("                /-*-/-*-/-*-/");
        System.out.println("                   /-*-/");
        
        // SUPERVIVIENTES
         listaSupervivientes = new ArrayList(Arrays.asList(new Superviviente("Francis"), new Superviviente("Louis"), new Superviviente("Nick"),
                 new Superviviente("Rochelle")));
        
        // asking new player his name and level dificulty --> name is static
        nivelDificultad = askNewPlayer();
        listaSupervivientes.add(new Superviviente(namePlayer));
        System.out.println("\n" + namePlayer + ", serás el LÍDER de los Supervivientes, tus decisiones tendrán repercusiones en el juego... Escoge bien...");
        System.out.println("Mucha suerte en tu viaje. La necesitarás.");
        
        // SKILLS
        ArrayList <SkillsPersonaje> listaSkills = new ArrayList(Arrays.asList(SkillsPersonaje.AMBIDIESTRO, SkillsPersonaje.FORTACHON, SkillsPersonaje.ESCURRIDIZO,SkillsPersonaje.RAPIDO, SkillsPersonaje.BUSCADOR));
        
        // SETTING SKILLS
        Collections.shuffle(listaSupervivientes);
        for(int i = 0; i < listaSupervivientes.size(); i++){
            listaSupervivientes.get(i).setSkill(listaSkills.get(i));
            if(listaSkills.get(i).equals(SkillsPersonaje.BUSCADOR)){
                posBuscador = i;
            }
        }
        
        // eleccion usuario de si abrir o no el cofre
        do{
            System.out.print("\nATENCIÓN! " + listaSupervivientes.get(posBuscador).getNombre() + " es un gran " +
                    listaSupervivientes.get(posBuscador).getSkill() + " y ha encontrado un cofre!\nLo abrimos? [0 - No | 1 - Sí]: ");
            abrirBaul = sc.nextInt();
        }while(abrirBaul != 0 && abrirBaul != 1);
        
        // setting listaArmas dependiendo del nivel y elección del usuario
        if(abrirBaul == 1){
            setArmasByGameLevel(nivelDificultad);
            if(nivelDificultad < 4){
                System.out.println("BUENA ELECCIÓN! Dentro hay armas con mucho polvo pero os servirán...\nSon estas: ");
            } else {
                System.out.println("Bueno bueno! Esto parece la cocina del Bulli, TODOS COGEIS UNA SARTEN...");
            }
        } else {
            setArmasByGameLevel(4);
            System.out.println("\nVAMOS MAL... Hemos perdido una gran oportunidad para sobrevivir...");
            System.out.println("Cerca hay una cocina... Hay SARTENES... Que dios os ayude...");
        }
        
        // SETTING ARMAS
        for(int i = 0; i < listaSupervivientes.size(); i++){
            if(abrirBaul == 1 && nivelDificultad < 4){
                System.out.print(listaArmas.get(i).getTipoArma() + " ");
            }
                
            listaSupervivientes.get(i).setArma(listaArmas.get(i));
            
            // si solo hay sartenes no hacemos comprobaciones
            if(nivelDificultad < 4){
                if(listaSupervivientes.get(i).getSkill().equals(SkillsPersonaje.RAPIDO)){
                    listaSupervivientes.get(i).getArma().setDanyo(2);
                }

                if(listaSupervivientes.get(i).getArma().getTipoArma().equals(TipoArma.PISTOLA) && !listaSupervivientes.get(i).getSkill().equals(SkillsPersonaje.AMBIDIESTRO)){
                   cambioDeArmas = true; 
                   posNoAmbidiestro = i;
                } else if (!listaSupervivientes.get(i).getArma().getTipoArma().equals(TipoArma.PISTOLA) && listaSupervivientes.get(i).getSkill().equals(SkillsPersonaje.AMBIDIESTRO) && nivelDificultad < 4){
                    cambioDeArmas = true;
                    armaAnterior = listaSupervivientes.get(i).getArma().getTipoArma();
                    posAmbidiestro = i;
                }
            }
        }
        
        if(cambioDeArmas){
            cambiosDeArmas(posNoAmbidiestro, posAmbidiestro, armaAnterior);
        } else {
            if(nivelDificultad < 4){
                System.out.println(listaSupervivientes.get(posDobleTurno).getNombre() + " tiene doble turno por ser AMBIDIESTRO y tener DOS PISTOLAS");
            } else {
                System.out.println("Matar zombies no sé pero hambre no pasareis!");
            }
        }
        
        Collections.shuffle(listaSupervivientes);
        printSupervivientes();
         
        return nivelDificultad;
    }
    
    public static void setArmasByGameLevel(int nivelDificultad){
        switch(nivelDificultad){
                case 1:
                    listaArmas = new ArrayList(Arrays.asList(new Arma(TipoArma.HACHA), new Arma(TipoArma.PISTOLA), new Arma(TipoArma.ESCOPETA), new Arma(TipoArma.HACHA), new Arma(TipoArma.KATANA)));
                    break;
                // MEDIUM
                case 2:
                    listaArmas = new ArrayList(Arrays.asList(new Arma(TipoArma.SARTEN), new Arma(TipoArma.PISTOLA), new Arma(TipoArma.ESCOPETA), new Arma(TipoArma.HACHA), new Arma(TipoArma.KATANA)));
                    break;
                // HARD
                case 3:
                    listaArmas = new ArrayList(Arrays.asList(new Arma(TipoArma.HACHA), new Arma(TipoArma.PISTOLA), new Arma(TipoArma.ESCOPETA), new Arma(TipoArma.SARTEN), new Arma(TipoArma.KATANA)));
                    break;
                case 4:
                    for(int i = 0; i < listaSupervivientes.size(); i++){
                        listaArmas.add(new Arma(TipoArma.SARTEN));
                    }
                    break;
        }
    }
    
    public static void printSupervivientes(){
        // PRINT SUPERVIVIENTES Y ZOMBIES
        System.out.println("\nLa cosa quedaría así: ");
        for(int i = 0; i < listaSupervivientes.size(); i++){
            System.out.println("Superviviente "+(i+1)+": ");
            System.out.println(listaSupervivientes.get(i).toString() + "\n");
        }
    }
    
    public static void cambiosDeArmas(int posNoAmbidiestro, int posAmbidiestro, TipoArma armaAnterior){
            int decision = -1;

            System.out.println("\n\nVaya, " + listaSupervivientes.get(posAmbidiestro).getNombre() + " es AMBIDIESTRO pero no tiene " + listaSupervivientes.get(posNoAmbidiestro).getArma().getTipoArma()
                    + "... La tiene " + listaSupervivientes.get(posNoAmbidiestro).getNombre() +".");
            do{
                System.out.print("Quieres hacer el cambio para que " + listaSupervivientes.get(posAmbidiestro).getNombre() + " tenga doble turno? [0 - NO | 1 - SI ]: ");
                decision = sc.nextInt();
            }while(decision < 0 || decision > 1);

            if(decision == 1){
                listaSupervivientes.get(posNoAmbidiestro).getArma().setTipoArma(armaAnterior);
                listaSupervivientes.get(posAmbidiestro).getArma().setTipoArma(TipoArma.PISTOLA);
                System.out.println("ASÍ SE HACE! Esto es trabajo en equipo! " + listaSupervivientes.get(posAmbidiestro).getNombre() + " recibe la " + listaSupervivientes.get(posAmbidiestro).getArma().getTipoArma() + ""
                        + " y ahora tiene DOBLE TURNO!");
                System.out.println(listaSupervivientes.get(posNoAmbidiestro).getNombre() + " se queda con la " +listaSupervivientes.get(posNoAmbidiestro).getArma().getTipoArma() + " "
                        + "que tenía antes " + listaSupervivientes.get(posAmbidiestro).getNombre() +".");
            } else {
                System.out.println("Qué poco compañerismo... Así vais por mal camino!");
            }
    }
    
    public static int askNewPlayer(){
        int option = 0, confirmacion = 0;
        boolean checker = false;
        do{
            checker = false;
            System.out.print("\nEscribe nombre jugador: ");
            namePlayer = sc.next();
            for(int i= 0; i <listaSupervivientes.size(); i++){
                if(listaSupervivientes.get(i).getNombre().equalsIgnoreCase(namePlayer)){
                    checker = true;
                    System.out.println("");
                }
            }
            if(checker){
                System.out.println("Hay un compañero de tu equipo que ya tiene un ese nombre. Son estos: ");
                for(int i= 0; i <listaSupervivientes.size(); i++){
                    System.out.print(listaSupervivientes.get(i).getNombre() + " ");
                }
            }
        }while(checker);
            
        do{
            System.out.println("\nNiveles de dificultad: ");
            System.out.println("1. EASY");
            System.out.println("2. MEDIUM");
            System.out.println("3. HARD");
            System.out.println("4. IMPOSSIBLE... Sólo las leyendas podrán sobrevivir! (Troll)");
            System.out.print("Escribe tu elección de nivel de dificultad: ");
            option = sc.nextInt();
            
            do{
                System.out.print("Has elegido el nivel " + option + " estás seguro? [0 - No | 1 - Sí]: ");
                confirmacion = sc.nextInt();
            }while(confirmacion != 0 && confirmacion != 1);
            
            if(confirmacion == 0){
                System.out.println("Está bien, repetimos...");
                option = -1;
            }            
        }while(option < 1 || option > 4);
        
        return option;
    }
    
    public static void creatingZombieListByLevel(int planta, int nivelDificultad){
        
        int numZombies = 8;
        listaZombies.clear();
        switch(nivelDificultad){
            case 1:
                numZombies = 6;
                switch(planta){
                    case 0:
                    case 1:
                    case 2:
                    for(int i = 0; i < numZombies; i++){
                        listaZombies.add(new Caminante());
                    }
                    break;
                }
                break;
            case 2:
                numZombies = 8;
                setZombiesByFloor(planta, numZombies);
                break;
            case 3:
                numZombies = 12;
                setZombiesByFloor(planta, numZombies);
                break;
            case 4:
                numZombies = 16;
                switch(planta){
                    case 0:
                    case 1:
                    case 2:
                    for(int i = 0; i < numZombies; i++){
                        listaZombies.add(new Witch());
                    }
                    break;
                }
                break;
        }
    }
    
    public static void setZombiesByFloor(int planta, int numZombies){
        switch(planta){
            // ZOMBIES PLANTA 1
            case 0:
                for(int i = 0; i < numZombies; i++){
                    listaZombies.add(new Caminante());
                }
                break;
            // ZOMBIES PLANTA 2
            case 1:
                for(int i = 0; i < numZombies; i++){
                    switch(MetodosSueltos.generaNumAleatorio(0, 1)){
                        case 0:
                            if(listaZombies.size() < numZombies){
                                listaZombies.add(new Caminante());
                            } else {
                                i = numZombies;
                            }
                            break;
                        case 1:
                            if(listaZombies.size() < numZombies - 2) {
                                listaZombies.add(new Gordo());
                                listaZombies.add(new Caminante());
                                listaZombies.add(new Caminante());
                            } else if(listaZombies.size() < numZombies){
                                listaZombies.add(new Caminante());
                            } else if(listaZombies.size() == numZombies){
                                i = numZombies;
                            }
                            break;
                    }
                }
                Collections.shuffle(listaZombies);
                break;
            // ZOMBIES PLANTA 3
            case 2:
                listaZombies.add(new Witch());
                for(int i = 0; i < numZombies - 1; i++){
                    switch(MetodosSueltos.generaNumAleatorio(0, 1)){
                        case 0: 
                            if(listaZombies.size() < numZombies){
                                listaZombies.add(new Caminante());
                            } else {
                                i = numZombies;
                            }
                            break;
                        case 1:
                            if(listaZombies.size() < numZombies - 2) {
                                listaZombies.add(new Gordo());
                                listaZombies.add(new Caminante());
                                listaZombies.add(new Caminante());
                            } else if(listaZombies.size() < numZombies){
                                listaZombies.add(new Caminante());
                            } else if(listaZombies.size() == numZombies){
                                i = numZombies;
                            }
                            break;
                    }
                }
                Collections.shuffle(listaZombies);
                break;
        }
        
    }
    
    public static void llenarArrayCharZombies(){
        arrayZombies = new char[listaZombies.size()];
        for(int i = 0; i < listaZombies.size(); i++){
            arrayZombies[i] = listaZombies.get(i).getClass().getSimpleName().charAt(0);
        }
    }
    
    public static void printArrayCharZombies(){
        System.out.println("Estado Zombies");
        for(int i = 0; i < arrayZombies.length; i++){
            System.out.print(arrayZombies[i] + "   |\t");
            
        }
        System.out.println("");
        for(int i = 0; i < arrayZombies.length; i++){
            System.out.print(i + "   |\t");
        }
        System.out.println("");
    }
    
    public static void llenarArrayCharSupervivientes(){
        arraySuperviv = new char[listaSupervivientes.size()];
        for(int i = 0; i < listaSupervivientes.size(); i++){
            if(listaSupervivientes.get(i).getNombre().equalsIgnoreCase(namePlayer)){
                arraySuperviv[i] = 'J';
            } else {
                arraySuperviv[i] = 'S';
            }        
        }
    }
    
    public static void printArrayCharSupervivientes(){
        System.out.println("Estado supervivientes: ");
        for(int i = 0; i < arraySuperviv.length; i++){
            if(arraySuperviv[i] == 'J'){
                System.out.print("S*  |\t");
            } else {
                System.out.print(arraySuperviv[i] + "   |\t");
            } 
        }
        System.out.println("");
    }
    
    public static void printEstadisticasSupervivientes(int humanosMatados){
        int maxKillsZombies = 0, minKillsZombies = 1000000, maxFallos = 0, minFallos = 1000000, posMuerto = 0;
        String nombreMaxKills = " ", nombreMinKills = " ", nombreMaxFallos = " " , nombreMinFallos = " ";
        boolean checker = false;
        
        System.out.println("\nESTADÍSTICAS FINALES de la partida: ");
        for(int i = 0; i < listaSupervivientes.size(); i++){
            if(listaSupervivientes.get(i).getZombiesMatados() > maxKillsZombies){
                nombreMaxKills = listaSupervivientes.get(i).getNombre();
                maxKillsZombies = listaSupervivientes.get(i).getZombiesMatados();
            }
            if(listaSupervivientes.get(i).getZombiesMatados() < minKillsZombies){
                nombreMinKills = listaSupervivientes.get(i).getNombre();
                minKillsZombies = listaSupervivientes.get(i).getZombiesMatados();
            }
             if(listaSupervivientes.get(i).getNumFallos() > maxFallos){
                nombreMaxFallos = listaSupervivientes.get(i).getNombre();
                maxFallos = listaSupervivientes.get(i).getNumFallos();
            }
            if(listaSupervivientes.get(i).getNumFallos() < minFallos){
                nombreMinFallos = listaSupervivientes.get(i).getNombre();
                minFallos = listaSupervivientes.get(i).getNumFallos();
            }
            if(!checker){
                if(listaSupervivientes.get(i).getVidas() < 1){
                    posMuerto = i;
                }
                checker = true;
            }
             
        }
        if(humanosMatados == 0){
            System.out.println("\nNi os habéis despeinado, no ha habido ninguna baja! Qué fácil no?!");
        } else if(humanosMatados == 1) {
            if(!listaSupervivientes.get(posMuerto).getNombre().equalsIgnoreCase(namePlayer)){
                System.out.println("Sólo ha muerto " + listaSupervivientes.get(posMuerto).getNombre() + "... Que descanse en paz...");
            } else {
                System.out.println("Qué mala suerte, sólo ha muerto " + humanosMatados + " superviviente y tenías que ser tu...");
            }
        } else if (humanosMatados > 1 && humanosMatados < 5){
            System.out.println("Recuento de bajas. Han muerto: " + humanosMatados + " supervivientes. Que descansen en paz...");
        }
        
        if(maxKillsZombies > 0){
            System.out.println("- El jugador que MÁS ZOMBIES ha matado es " + nombreMaxKills + " con " + maxKillsZombies + " zombies. Toda una máquina de matar!");
            if(!nombreMinKills.equalsIgnoreCase(namePlayer)){
                System.out.println("- El jugador que MENOS ZOMBIES ha matado es " + nombreMinKills + " con " + minKillsZombies + " zombies. Entre nosotros... un poco lastre...");
            } else {
                System.out.println("- El jugador que MENOS ZOMBIES ha matado eres tu, " + nombreMinKills + ", con " + minKillsZombies + " zombies. Habrá que entrenar más duro.");
            }
            System.out.println("- El jugador que MÁS ATAQUES ha fallado es " + nombreMaxFallos + " con " + maxFallos + " fallos."
                    + " Deberíamos afinar la puntería el próximo día!");
            System.out.println("- El jugador que MENOS ATAQUES ha fallado ha matado es " + nombreMinFallos + " con " + minFallos + " fallos. Este tio es más fiable que Larry Bird tirando triples.");
        } else {
            System.out.println("Estadísticas? El resumen es que las WITCH os han dado una buena paliza y se están pegando un hartón de reir.");
            System.out.println("Estas dicen: 'Lo importante es participar'.");
        }
    
    }
    
    public static String mensajeFallo(Superviviente player){
        String mensaje = "";
        switch(player.getArma().getTipoArma()){
            case ESCOPETA: 
                mensaje = "\tVAYA... Esta " + TipoArma.ESCOPETA + " falla más que una de feria, nunca mejor dicho...";
                break;
            case HACHA:
                mensaje = "\tLa " + TipoArma.HACHA + " pesa demasiado para " + player.getNombre() + " y se ha dado en el pie... Gimli se sentiría avergonzado...";
                break;
            case KATANA:
                mensaje = "\tLa " + TipoArma.KATANA +" no está lo suficiente afilada para matar al Zombie pero si para cortarse un dedo... Los samurais estan de luto...";
                break;
            case PISTOLA: 
                mensaje = "\tSe ha encasquillado la " +TipoArma.PISTOLA + " y ha fallado el ataque. Clint Eastwood se hecha las manos a la cabeza...";
                break;
            case SARTEN:
                mensaje = "\tNo sé qué es más vergonzoso si tener una " +TipoArma.SARTEN +" de arma o no saberla usar... Llamaremos a Gordon Ramsay para que os dé unos consejos...";
        }
        return mensaje;
    }
}
