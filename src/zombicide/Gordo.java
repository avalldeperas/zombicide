package zombicide;

public class Gordo extends Zombie {
    private static int GOLPEOMAXIMOGORDO;
    
    public Gordo (){
        super(1, 2, TipoZombie.GORDO, 2);
    }

    public Gordo(int danyo, int movimiento, TipoZombie tipo) {
        super(danyo, movimiento, tipo.GORDO, 2);
    }

    public static int getGolpeMaximoGordo() {
        return GOLPEOMAXIMOGORDO;
    }

    public static void setGolpeMaximoGordo(int golpeMaximoGordo) {
        Gordo.GOLPEOMAXIMOGORDO = golpeMaximoGordo;
    }
    
    @Override
    public int calcularGolpeo(int numAleatorio, Superviviente superviv) {
        boolean maxZombies = false, maxGordo = false;
        int golpeo = super.calcularGolpeo(numAleatorio, superviv)+ 2 * getMovimiento(); //To change body of generated methods, choose Tools | Templates.
        
        // GOLPEO MÁXIMO DE TODOS LOS ZOMBIES
        if(golpeo > getMaxGolpeo()){
            setMaxGolpeo(golpeo);
            maxZombies = true;
        }
        // GOLPEO MÁXIMO DE GORDOS
        if(golpeo > getGolpeMaximoGordo()){
            setGolpeMaximoGordo(golpeo);
            maxGordo = true;
        }
        
        if(maxGordo && maxZombies){
            System.out.println("\tEl " + getClass().getSimpleName() + " ha establecido un nuevo récord de golpeo de TODOS los ZOMBIES!");
        } else if (maxGordo && !maxZombies){
            System.out.println("\tEl " + getClass().getSimpleName() + " ha establecido un nuevo récord de todos los " + getClass().getSimpleName());
        }
        
        return golpeo;
    }

    @Override
    public int getVidas() {
        return super.getVidas(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
