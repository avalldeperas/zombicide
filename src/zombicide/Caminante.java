package zombicide;

public class Caminante extends Zombie{
    private static int GOLPEOMAXIMOCAMINANTE;
    
    public Caminante(){
        super(1, 1, TipoZombie.CAMINANTE, 1);
    }

    public Caminante(int danyo, int movimiento, TipoZombie tipo) {
        super(danyo, movimiento, tipo.CAMINANTE, 1);
    }
    
    public int getGolpeoMaximoCaminante() {
        return GOLPEOMAXIMOCAMINANTE;
    }

    public void setGolpeoMaximoCaminante(int golpeo) {
        GOLPEOMAXIMOCAMINANTE = golpeo;
    }
    
    @Override
    public int getVidas() {
        return super.getVidas(); //To change body of generated methods, choose Tools | Templates.
    }

    // EL GET I SET DE MAX GOLPEO SE PUEDE HACER EN ZOMBIE??
    @Override
    public int calcularGolpeo(int numAleatorio, Superviviente superviv) {
        boolean maxZombies = false, maxCaminantes = false;
        int golpeo = super.calcularGolpeo(numAleatorio, superviv) + 5;
        // GOLPEO MAXIMO DE TODOS LOS ZOMBIES
        if(golpeo > getMaxGolpeo()){
            setMaxGolpeo(golpeo);
            maxZombies = true;
        }
        
        // GOLPEO MAXIMO DE CAMINANTES
        if(golpeo > getGolpeoMaximoCaminante()){
            setGolpeoMaximoCaminante(golpeo);
            maxCaminantes = true;
        }
        
        if(maxCaminantes && maxZombies){
            System.out.println("\tEl CAMINANTE ha establecido un nuevo récord de golpeo de TODOS los ZOMBIES!");
        } else if (maxCaminantes && !maxZombies){
            System.out.println("\tEl CAMINANTE ha establecido un nuevo récord de todos los " + getClass().getSimpleName());
        }
        
        return golpeo; //To change body of generated methods, choose Tools | Templates.
    }  
    
    @Override
    public int getMaxGolpeo() {
        return super.getMaxGolpeo(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setMaxGolpeo(int maxGolpeo) {
        super.setMaxGolpeo(maxGolpeo); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
}
