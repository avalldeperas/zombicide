
package zombicide;

public class Superviviente {
    
    private String nombre;
    private int vidas;
    private int nivel;
    private SkillsPersonaje skill = null;
    private Arma arma = new Arma(TipoArma.SARTEN);
    private Arma armaSecundaria = null;
    private int numMordidas;
    private static boolean yaEscapado = false;
    private int numFallos;
    private int zombiesMatados;
    
    
    public Superviviente() {
        this.vidas = 3;
        this.nivel = 0;
    }
    
    public Superviviente(String nombre) {
        this.vidas = 3;
        this.nivel = 0;
        this.nombre = nombre;
    }

    public int getNumFallos() {
        return numFallos;
    }

    public void setNumFallos(int numFallos) {
        this.numFallos = numFallos;
    }

    public int getZombiesMatados() {
        return zombiesMatados;
    }

    public void setZombiesMatados(int zombiesMatados) {
        this.zombiesMatados = zombiesMatados;
    }

    public int getVidas() {
        return vidas;
    }

    public void setVidas(int vida) {
        if(vida < 0){
            this.vidas = 0;
        } else {
            this.vidas = vida;
        }
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public SkillsPersonaje getSkill() {
        return skill;
    }

    public void setSkill(SkillsPersonaje skill) {
        this.skill = skill;
        switch(this.skill){
            case RAPIDO:
                this.arma.setDanyo(2);
                break;
            case BUSCADOR:
                this.arma.setTipoArma(TipoArma.KATANA);
                break;
            case FORTACHON:
                setVidas(getVidas()+1);
                break;
            case ESCURRIDIZO:
                break;
            case AMBIDIESTRO:
                if(getArma().getTipoArma() == TipoArma.PISTOLA){
                    armaSecundaria = new Arma(TipoArma.PISTOLA);
                }
                break;
        }
    }

    public int getNumMordidas() {
        return numMordidas;
    }

    public void setNumMordidas(int numMordidas) {
        this.numMordidas = numMordidas;
    }
    
    public boolean tieneArma(){
        if(this.arma == null){
            return false;
        } else {
            return true;
        }
    }
    
    public void deleteArma(){
        this.arma = null;
    }

    @Override
    public String toString() {
        return "\tNombre: " + nombre + "\n\tVidas: " + vidas + "\n\t"
                + "Nivel: " + nivel + "\n\tSkill: " + skill + ""
                + "\n\tArma: " + arma.toString();
    }
    
    public void setArma(Arma arma) {
        this.arma = arma;
       
    }

    public Arma getArma() {
        return arma;
    }
    
    public boolean escaparse(){
        boolean sePuedeEscapar = false;
        if(getSkill().equals(SkillsPersonaje.ESCURRIDIZO)){
            if(!yaEscapado){
                sePuedeEscapar = true;
                setYaEscapado(true);
            } else {
                System.out.println("\t"+getNombre() + " ya se ha escapado una vez!");
            }
        }
        return sePuedeEscapar;
    }

    public boolean getYaEscapado() {
        return yaEscapado;
    }

    public void setYaEscapado(boolean yaEscapado) {
        this.yaEscapado = yaEscapado;
    }
    
    
}
